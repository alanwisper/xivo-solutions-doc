.. _xivosolutions_release:

*************
Release Notes
*************

.. _jabbah_release:

Jabbah
======

Below is a list of *New Features* and *Behavior Changes* compared to the previous LTS version, Izar (2022.05).

New Features
------------

**Assistants**

* Desktop Application 

  * Make Switchboard/POPC possible in Desktop Applications
  * Third party application displayed in Desktop Application

* UC Assistant

  * Call history: internal users status is now shown in history

**Meeting rooms**  

* Integration of hand raising and lowering

**XiVO PBX**

* Unique Account : UA users now works in call groups, group pickup, and boss secretary filter
* New option ``run_scripts`` was added to wizard API json. See :ref:`xivo_wizard_run_scripts` in Wizard page.
  Default is ``False``.
* New option ``handle_system_conf`` was added to wizard API json. See :ref:`xivo_wizard_handle_system_conf` in Wizard page.
  Default is ``True``.
* Labels: Improve web-interface labels with filtering and better display for long lists

**API**

* Call history API was enriched - see :ref:`xuc_api_history`
  

Behavior Changes
----------------

**XiVO PBX**

* Users in Groups and Queues:

  * The possibility to specify the *channel* (*default* or *Local*) for a user in a group or queue was removed.
  * All users in Groups and Queues are now added with interface ``Local/id-42@usercallback`` (for a user with user id 42)
  * All users that were Groups' or Queues' member will have the queue/group member interface changed from ``SIP/abcd`` or ``Local/1000@default`` to ``Local/id-42@usercallback`` during upgrade (for a user with SIP line abcd and number 1000 and user id 42).
  * Behavior change for XiVO unconditional forward (`*21`) and do not disturb (`*25`):

    +---------------+-------------------------------------------+----------------------------+
    | Group member  | Before (<=Izar)                           | After (>= Jabbah)          |
    +===============+===========================================+============================+
    |               | Channel type                              |  Channel type              |
    |               +------------------------+------------------+  is not applicable         |
    |               | *default*              | *Local*          |                            |
    +---------------+------------------------+------------------+----------------------------+
    | DND activated | User is called anyway  | DND is followed  | **User is not called**     |
    +---------------+------------------------+------------------+----------------------------+
    | UNC activated | User is called anyway  | UNC is followed  | **User is called anyway**  |
    +---------------+------------------------+------------------+----------------------------+

    +---------------+----------------------------------------------+----------------------------+
    | Queue member  | Before (<=Izar)                              | After (>= Jabbah)          |
    +===============+==============================================+============================+
    |               | Channel type                                 |  Channel type              |
    |               +------------------------+---------------------+  is not applicable         |
    |               | *default*              | *Local*             |                            |
    +---------------+------------------------+---------------------+----------------------------+
    | DND activated | User is called anyway  | User is not called  | **User is not called**     |
    +---------------+------------------------+---------------------+----------------------------+
    | UNC activated | User is called anyway  | User is not called  | **User is called anyway**  |
    +---------------+------------------------+---------------------+----------------------------+


* Directory Research: Lookup results are now ordered in way to prefer more relevant results (see :ref:`dird_plugin_view_defaultjson`)

* `*8` is not anymore supported by asterisk core feature but xivo-feature. Pay attention that migration sets it back to `*8`

**Meetingroom**

* MEETINGROOM_AUTH_DOMAIN should be updated from 'avencall.com' to either '*' or 'meet-jitsi'

Deprecations
------------

This release deprecates:

* `LTS Deneb (2019.12) <https://documentation.xivo.solutions/en/2019.12/>`_: This version is no longer supported.
  No bug fixes, no security update will be provided for this release.

Upgrade
-------

.. _upgrade_lts_manual_steps:

**Manual steps for LTS upgrade**

.. warning:: **Don't forget** to read carefully the specific steps to upgrade from another LTS version

.. toctree::
   :maxdepth: 1

   upgrade_from_five_to_polaris
   upgrade_from_polaris_to_aldebaran
   upgrade_from_aldebaran_to_borealis
   upgrade_from_borealis_to_callisto
   upgrade_from_callisto_to_deneb
   upgrade_from_deneb_to_electra
   upgrade_from_electra_to_freya
   upgrade_from_freya_to_gaia
   upgrade_from_gaia_to_helios
   upgrade_from_helios_to_izar
   upgrade_from_izar_to_jabbah


**Generic upgrade procedure**

Then, follow the generic upgrade procedures:

* :ref:`XiVO PBX upgrade procedure <upgrade>`
* :ref:`XiVO CC upgrade procedure <upgrade_cc>`
* :ref:`XDS upgrade procedure <upgrade_xds>`

Jabbah Bugfixes Versions
========================

Components version table
------------------------

Table listing the current version of the components.

+----------------------+----------------+
| Component            | current ver.   |
+======================+================+
| **XiVO**                              |
+----------------------+----------------+
| XiVO PBX             |  2022.10.00    |
+----------------------+----------------+
| config_mgt           |  2022.10.01    |
+----------------------+----------------+
| db                   |  2022.10.00    |
+----------------------+----------------+
| outcall              |  2022.10.00    |
+----------------------+----------------+
| db_replic            |  2022.10.00    |
+----------------------+----------------+
| nginx                |  2022.10.00    |
+----------------------+----------------+
| webi                 |  2022.10.00    |
+----------------------+----------------+
| switchboard_reports  |  2022.10.00    |
+----------------------+----------------+
| asterisk             | 18.10          |
+----------------------+----------------+
| docker-ce            | 5:20.10.13     |
+----------------------+----------------+
| docker-compose       | 1.29.2         |
+----------------------+----------------+
| **XiVO CC**                           |
+----------------------+----------------+
| elasticsearch        |  7.14.0        |
+----------------------+----------------+
| kibana               |  7.14.0        |
+----------------------+----------------+
| logstash             |  2022.10.00    |
+----------------------+----------------+
| mattermost           |  2022.10.00    |
+----------------------+----------------+
| nginx                |  2022.10.00    |
+----------------------+----------------+
| pack-reporting       |  2022.10.00    |
+----------------------+----------------+
| pgxivocc             |  2022.10.00    |
+----------------------+----------------+
| recording-rsync      |                |
+----------------------+----------------+
| recording-server     |  2022.10.01    |
+----------------------+----------------+
| spagobi              |  2022.10.00    |
+----------------------+----------------+
| xivo-full-stats      |  2022.10.01    |
+----------------------+----------------+
| xuc                  |  2022.10.01    |
+----------------------+----------------+
| xucmgt               |  2022.10.01    |
+----------------------+----------------+
| **Edge**             |                |
+----------------------+----------------+
| edge                 |  2022.10.00    |
+----------------------+----------------+
| nginx                |  2022.10.00    |
+----------------------+----------------+
| kamailio             |  2022.10.00    |
+----------------------+----------------+
| coturn               |  2022.10.00    |
+----------------------+----------------+
| **Meeting Rooms**    |                |
+----------------------+----------------+
| meetingroom          |  2022.10.00    |
+----------------------+----------------+
| web-jitsi            |  2022.10.00    |
+----------------------+----------------+
| jicofo-jitsi         |  2022.10.00    |
+----------------------+----------------+
| prosody-jitsi        |  2022.10.00    |
+----------------------+----------------+
| jvb-jitsi            |  2022.10.00    |
+----------------------+----------------+
| jigasi-jitsi         |  2022.10.00    |
+----------------------+----------------+
| **IVR**              |                |
+----------------------+----------------+
| ivr-editor           |  2022.10.01    |
+----------------------+----------------+


Jabbah.01
---------

Consult the `2022.10.01 (Jabbah.01) Roadmap <https://projects.xivo.solutions/versions/372>`_.

Components updated: 

Docker :

config-mgt,ivr-editor,recording-server,xivo-agid,xivo-confgend,xivo-full-stats,xucmgt,xucserver

Debian :

xivo-agid,xivo-confgend,xivo-config,xivo-dao,xivo-dxtora,xivo-dxtorc,xivo-provisioning,xivocc-installer

**Asterisk**

* `#5890 <https://projects.xivo.solutions/issues/5890>`_ - Sometimes call is automatically hangup when answered by mobile application
* `#5930 <https://projects.xivo.solutions/issues/5930>`_ - [C] PJSIP "Insecure" option in the SIP Trunk configuration 

**Desktop Assistant**

* `#5875 <https://projects.xivo.solutions/issues/5875>`_ - Desktop application - systray menu is not working on Linux

**Mobile Application**

* `#5577 <https://projects.xivo.solutions/issues/5577>`_ - As mobile app user I want to correctly see missed calls on my webapp and mobileapp
* `#5802 <https://projects.xivo.solutions/issues/5802>`_ - UA User - Be able to use the mobile app with UA user

**Reporting**

* `#5906 <https://projects.xivo.solutions/issues/5906>`_ - Handle call history when user is WebAppAndMobileApp (follow up of #5890)
* `#5938 <https://projects.xivo.solutions/issues/5938>`_ - xivo-full-stats restart indefinitely if CEL with appdata contains chars different than [a-zA-Z]

**Switchboard**

* `#5871 <https://projects.xivo.solutions/issues/5871>`_ - Switchboard - missing link to download app
* `#5872 <https://projects.xivo.solutions/issues/5872>`_ - [Doc] - Update DApp users' guide with Switchboard

**Web Assistant**

* `#5870 <https://projects.xivo.solutions/issues/5870>`_ - Call history buttons appear out of the div
* `#5873 <https://projects.xivo.solutions/issues/5873>`_ - [Doc] - UPdate users' guide with new call history
* `#5907 <https://projects.xivo.solutions/issues/5907>`_ - Update xucmgt to use missed_call from user preferences instead of computing it.

**XUC Server**

* `#5877 <https://projects.xivo.solutions/issues/5877>`_ - Add log in xuc with the connection type when a user is log in

**XiVO PBX**

* `#5249 <https://projects.xivo.solutions/issues/5249>`_ - IVR uploads new audio file in place of an existing error.
* `#5886 <https://projects.xivo.solutions/issues/5886>`_ - PJSIP - Wrong option mapping for directmedia = nonat in sip config
* `#5895 <https://projects.xivo.solutions/issues/5895>`_ - Users state in group is not correctly taken into account
* `#5908 <https://projects.xivo.solutions/issues/5908>`_ - Store the number of missed calls in user preferences
* `#5933 <https://projects.xivo.solutions/issues/5933>`_ - As a user I want to have an error displayed when trying to login on MobileApp if XiVO is not properly configured for it
* `#5947 <https://projects.xivo.solutions/issues/5947>`_ - XDS - agid doesn't start on MDS (python build problem)

**XiVO Provisioning**

* `#5822 <https://projects.xivo.solutions/issues/5822>`_ - dxtorc and dxtora do not work in python 3
* `#5904 <https://projects.xivo.solutions/issues/5904>`_ - TFTP server wrong encoding


Jabbah.00
---------

Consult the `2022.10.00 (Jabbah.00) Roadmap <https://projects.xivo.solutions/versions/292>`_.

Components updated: 

Docker :

xivo-jicofo-jitsi
xivo-jvb-jitsi
xivo-prosody-jitsi
xivo-web-jitsi
xucmgt

Debian :

ivr-editor
xivo-agid
xivo-config
xivo-jigasi-jitsi
xivo-meetingrooms
xivo-provd-plugins
xivo-web-interface

**Desktop Assistant**

* `#5111 <https://projects.xivo.solutions/issues/5111>`_ - Desktop App - Use gif when installing the app on Windows.

**Mobile Application**

* `#5800 <https://projects.xivo.solutions/issues/5800>`_ - Mobile App - Allow call group, BS filter and call pickup

**Visioconf**

* `#4558 <https://projects.xivo.solutions/issues/4558>`_ - Conference Room - Reenable the Meeting Participant View
* `#4731 <https://projects.xivo.solutions/issues/4731>`_ - Meeting Room - Unable to un-mute audio only participant if muted
* `#4770 <https://projects.xivo.solutions/issues/4770>`_ - Integration of hand raising and lowering
* `#5794 <https://projects.xivo.solutions/issues/5794>`_ - Update jitsi containers

  .. important:: **Behavior change** MEETINGROOM_AUTH_DOMAIN should be updated from 'avencall.com' to either '*' or 'meet-jitsi'


**Web Assistant**

* `#5773 <https://projects.xivo.solutions/issues/5773>`_ - Ucassistant history icon tooltips not showing or are wrong
* `#5809 <https://projects.xivo.solutions/issues/5809>`_ - UC assistant : bugs on call history page 

**XiVO PBX**

* `#5770 <https://projects.xivo.solutions/issues/5770>`_ - IVR build breaks when trying to release
* `#5784 <https://projects.xivo.solutions/issues/5784>`_ - deleting line of user in a group and queue
* `#5801 <https://projects.xivo.solutions/issues/5801>`_ - UA Users in Call Groups (or queues) - Be able to pass on the group/queue option to the actual dialed peer
* `#5818 <https://projects.xivo.solutions/issues/5818>`_ - Boss/Secretary filter "Ringing time" field missing in specific scenario
* `#5867 <https://projects.xivo.solutions/issues/5867>`_ - Penalty Field for queue is offset in edit user form/groups

**XiVO Provisioning**

* `#5398 <https://projects.xivo.solutions/issues/5398>`_ - [C] - Provisioning - Directed call pickup doesn't work on EXP50 + T57W - xivo-yealink-v85 plugin

**XiVOCC Infra**

* `#5544 <https://projects.xivo.solutions/issues/5544>`_ - Frontend Integration Tests - Be able to run cypress test locally
 

Jabbah Intermediate Versions
============================

.. toctree::
   :maxdepth: 2

   jabbah_iv
