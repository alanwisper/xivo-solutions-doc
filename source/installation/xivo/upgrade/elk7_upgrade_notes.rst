.. _elk7_upgrade_notes:

*******************
ELK 7 Upgrade Notes
*******************

ELK was upgraded to version 7 in XiVO 2019.10 release.

Data/Dashboard of previous Elastic/Kibana version:

* were backuped before upgrade
* but **were not upgraded**.

This page describes the procedure to

#. activate the old Elastic/Kibana version
#. so it can be used during the time the dashboard are created in the new Kibana
#. and then how to remove backup and old Elastic/Kibana version

.. _elk7_previous_version_totem_panels:

Use Previous Version of Totem Panels
====================================

With Deneb, new version of the ELK stack is included. The old Kibana panels are disabled, but you can still reactive
them manually while loosing access to the new one. To do so, you have to rename the docker compose override file
on the *XiVO CC* server and recreate services:

.. code-block:: bash

  mv /etc/docker/compose/docker-xivocc.override.yml.elk17 /etc/docker/compose/docker-xivocc.override.yml
  xivocc-dcomp up -d

These commands will restart the **old** Elasticsearch and Kibana on the XiVOCC.

Then, on *XiVO PBX*, you need to edit the :file:`/etc/docker/xivo/docker-xivo.override.yml` file to reactivate the replication from ``db_replic`` to Elasticearch.

.. code-block:: diff

  -   - DISABLEELASTIC
  +   - DISABLEELASTIC=false

Then you'll need to do a ``xivo-dcomp up -d``.
This will reacreate the ``db_replic`` on the *XiVO PBX*.

You'll then be able to access the data using the URL http://XIVOCC_SERVER_ADDRESS/prevKibana

.. important::
  While using the previous version of Totem Panels, these are not accessible from the fingerboard, only through
  the http://XIVOCC_SERVER_ADDRESS/prevKibana link.


Put Back the New ELK 7
======================

To put back the new version of ELK:

* On *XiVO CC*, remove the override file::

   rm /etc/docker/compose/docker-xivocc.override.yml

* and re-create the containers::
  
   xivocc-dcomp up -d --remove-orphans

* On *XiVO PBX*, set the ``DISABLEELASTIC`` env variable to ``true`` directly in the :file:`/etc/docker/xivo/docker-xivo.override.yml`::

   - DISABLEELASTIC=true

* and re-create the containers::
  
   xivo-dcomp up -d


Cleaning Backup of Old Dashboards
=================================

If you're not going to use the previous version of the Elasticsearch or if you're done creating the Dashboard in the new Kibana, please delete the folder with backuped data (on *XiVO CC*):

.. code-block:: bash

  rm -rf /var/local/elasticsearch-1.7/


