.. highlightlang:: rest

.. _callbacks:

*********
Callbacks
*********

Introduction
============

The goal of the callback system is to be able to perform scheduled outgoing calls.
These requests can be completed with specific information such as a description or a personal name.

The core object of the callback system is the **callback request**. A callback request is made of the following fields:

- First name of person to call
- Last name
- Phone number
- Mobile phone number
- Company name
- Description
- Due date

Each callback request is associated to a predefined **callback period**, which represents the preferred interval of the day
in which the call should be performed.

A callback request cannot exist on its own: it must be stored in a **callback list**, which is itself associated to a queue.

Once a callback request has been performed, it generates a **callback ticket**. This ticket sums up the original information of the callback request,
but adding some new fields:

- Start date: date at which the callback request was actually performed
- Last update: date of the last modification of the ticket
- Comment
- Status : the result of the callback
- Agent: the Call Center agent who performed the callback

.. _callback_lists:

Callback Lists
==============

A callback list is an object which will contain callback request. It is associated to a queue,
and several callback lists can be associated to the same queue. Callback lists are created using the **configuration manager**

.. figure:: callback_lists.png
    :scale: 80%

Once created, a list can be populated whether through the :ref:`Callbacks tab <callbacks_with_ccmanager>` of the CCManager, or programmatically
through the web services of the configuration server.

.. _callback_periods:

Callback Periods
================

A callback period represents an interval of the day, bounded by a start date and an end date. It can be set as the default interval, so that
a newly created callback request will be associated to this period if none is specified.

.. figure:: callback_periods.png
    :scale: 80%

.. _callbacks_with_ccmanager:

Managing Callbacks Using CCManager
==================================

Using the CCManager callback view you may import a list of callbacks, monitor callback completion and download the associated tickets.

.. figure:: ccmanager_callbacks.png
    :scale: 80%

Importing Callbacks
-------------------

Callbacks can be imported from a CSV file into a :ref:`callback list <callback_lists>`.

Line delimiter must be a new line character and column separator must be one of: '|' or ',' or ';'. Columns can be optionaly enclosed by double-quote '"'.

The file must look like the following:

::

    phoneNumber|mobilePhoneNumber|firstName|lastName|company|description|dueDate|period
    0230210092|0689746321|John|Doe|MyCompany|Call back quickly||
    0587963214|0789654123|Alice|O'Neill|YourSociety||2016-08-01|Afternoon

The header line must contain the exact field named described below:

+-------------------+-----------------------------------------------------------------------------------+
| Field Name        | Description                                                                       |
+===================+===================================================================================+
| phoneNumber       | The number to call (at least either phoneNumber or mobilePhoneNumber is required) |
+-------------------+-----------------------------------------------------------------------------------+
| mobilePhoneNumber | Alternate number to call                                                          |
+-------------------+-----------------------------------------------------------------------------------+
| firstName         | The contact first name (optional)                                                 |
+-------------------+-----------------------------------------------------------------------------------+
| lastName          | The contact last name (optional)                                                  |
+-------------------+-----------------------------------------------------------------------------------+
| company           | The contact company (optional)                                                    |
+-------------------+-----------------------------------------------------------------------------------+
| description       | A text that will appear on the agent callback pane                                |
+-------------------+-----------------------------------------------------------------------------------+
| dueDate           | The date when to callback, using ISO format: YYYY-MM-DD,                          |
|                   | ie. 2016-08-01 for August, 1st, 2016.                                             |
|                   | If not present the next day will be used as dueDate (optional)                    |
+-------------------+-----------------------------------------------------------------------------------+
| period            | The name of the period as defined in :ref:`callback list <callback_lists>`.       |
|                   | If not present, the default period will be used (optional)                        |
+-------------------+-----------------------------------------------------------------------------------+

When an agent takes a callback, the column ``Taken by`` is updated with the number of the agent. The callback disappears when it is processed.

Exporting Callbacks Tickets
---------------------------

The tickets of the processed callbacks can be downloaded by clicking on the ``Download tickets`` button.

The downloaded file is a csv file with the comma ',' as delimiter.

.. _callbacks_with_ccagent:

Processing Callbacks with CCAgent
=================================

The agent can see the :ref:`callbacks <callbacks>` related to the queues he is logged on.
They are available in the ``Callbacks`` menu.
A notification is also shown to display pending callbacks in menu to know status at any time and from any other screen of the application.

On this page, the agent only has access to basic information about the callback: activity and due date,
On the left of each callback line, a colored clock indicates the temporal status of this callback:

- `yellow` if the callback is to be processed later
- `green` if we are currently inside the callback period
- `red` if the callback period is over

.. figure:: ccagent-callbacks-view.png
    :scale: 80%

To process one of these callbacks, the agent must click on one of the callbacks line.

.. figure:: ccagent-callbacks-edit.png
    :scale: 80%

To launch the call, the agent must click on one of the available phone numbers.

Once the callback is launched, the status can be changed and a comment can be added.

.. figure:: ccagent-callbacks-status.png
    :scale: 80%

If you set 'To reschedule' as status, the callback can be rescheduled at a later time and another period:

  .. figure:: ccagent-callbacks-reschedule.png
      :scale: 80%

Other statuses are available to be set and will close callback once saved :

      - *Answered* if caller accepted the call
      - *NoAnswer* if caller were unreachable
      - *Fax* if callback has been resolved thanks to a Fax message
      - *Handled by mail* if callback has been resolved thanks to an E-mail

Clicking on the calendar icon next to the "New due date" field, will popup a calendar to select another callback date.
