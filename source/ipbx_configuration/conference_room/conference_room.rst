.. _conference_configuration:

***************
Conference Room
***************

Adding a conference room
========================

In this example, we'll add a conference room with number 4010.

First, you need to define a conference room number range for the ''default'' context via the :menuselection:`Services --> IPBX --> IPBX Configuration --> Contexts` page.

.. figure:: images/Confroom_edit_context.png
   :scale: 85%
   
   Adding a conference room number range to the default context

You can then create a conference room via the :menuselection:`Services --> IPBX --> IPBX Settings --> Conference rooms` page.

.. figure:: images/Confroom_create.png
   :scale: 85%
   
   Creating conference room 4010

In this example, we have only filled the ''Name'' and ''Number'' fields, the others have been left to their default value.

As you can see, there's quite a few options when adding / editing a conference room. Here's a description of the most common one:

General Tab
-----------

* :guilabel:`Pin code`: Protects your conference room with a PIN number. People trying to join the room will be asked for the PIN code.
* :guilabel:`Organizer Pin Code`: This pin code is used to define an organizer for the conference, organizer will have the right to mute
  or kick participants with the :ref:`uc-assistant_conferences` monitoring in uc assistant application.
* :guilabel:`Waiting room`: Once this option is checked the users are not able to speak in the room and have to wait the organizer
  to join the conference.

.. note:: You must define a general pin code if you want to be able to organize conferences and use the organizers features.

Advanced Tab
------------

.. figure:: images/Confroom_advanced.png
   :scale: 85%
   
   Conference room configuration advanced tab

* :guilabel:`Don't play announce for first participant`: First user joining will not get any announce.
* :guilabel:`No incoming notification`: Users will not hear incoming announces. Deactivates the sound when user enters / leaves conference.
  It also deactivates the name recording feature, but not the announce of number of participants.
* :guilabel:`Announce number of participants`: Announce user(s) count on joining a conference.
* :guilabel:`Record name and announce when joining and leaving`: The user is allowed to record his name that will be heard by the other
  conference attendees. Can be reviewed afert record (yes) or not (No Review).
* :guilabel:`Recording`: This conference room is recorded.
* :guilabel:`Max participants`: Define the maximum number of participants, default to 0.
* :guilabel:`Preprocess subroutine`: See :ref:`subroutine`.

