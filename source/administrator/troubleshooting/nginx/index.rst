*****************
NGINX - proxy web
*****************

Basic check
===========

On the standard HTTP port of the machine (80) you are redirected to UC Assistant over HTTPS (443).

You can open the fingerboard page https://xivocc/fingerboard

Docker says nginx is restarting
-------------------------------
* Check logs for missing files or links, nginx refuses to start if one of servers is not accessible, e.g. xuc is down.
