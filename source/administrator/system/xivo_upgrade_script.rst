.. _xivo-upgrade_script:

*******************
xivo-upgrade script
*******************

Usage
=====

.. note::
   * You can't use xivo-upgrade if you have not run the wizard yet
   * Upgrading from a version prior to *XiVO PBX* 1.2 is not supported.
   * When upgrading XiVO, you **must** also upgrade **all** associated XiVO
     Clients. There is currently no retro-compatibility on older *XiVO PBX* Client
     versions.

This script will update *XiVO PBX* and restart all services.

There are 2 options you can pass to xivo-upgrade:

* ``-d`` to only download packages without installing them. **This will still upgrade the package containing xivo-upgrade and xivo-service**.
* ``-f`` to force upgrade, without asking for user confirmation

``xivo-upgrade`` uses the following environment variables:

* ``XIVO_CONFD_PORT`` to set the port used to query the :ref:`HTTP API of xivo-confd <confd-api>`
  (default is 9486)


Troubleshooting
===============

Postgresql
----------

When upgrading XiVO, if you encounter problems related to the system locale, see
:ref:`postgresql_localization_errors`.


xivo-upgrade
------------

If xivo-upgrade fails or aborts in mid-process, the system might end up in a faulty condition. If in
doubt, run the following command to check the current state of xivo's firewall rules::

   iptables -nvL

If, among others, it displays something like the following line (notice the DROP and 5060)::

   0     0 DROP       udp  --  *      *       0.0.0.0/0            0.0.0.0/0           udp dpt:5060

Then your XiVO will not be able to register any SIP phones. In this case, you must delete the DROP
rules with the following command::

   iptables -D INPUT -p udp --dport 5060 -j DROP

Repeat this command until no more unwanted rules are left.
