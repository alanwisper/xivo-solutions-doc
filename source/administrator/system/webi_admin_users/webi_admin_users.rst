.. _webi_admin_users:

****************
Webi admin users
****************

Webi users with limited rights can be configured in the :menuselection:`Configuration --> Management --> Users` menu.

.. figure:: webi_users_menu.png

  :menuselection:`Configuration --> Management --> Users`

Rules
=====

Newly created webi user can't see any webi menus. To add them, use the `key` action button.
Then choose objects which can be edited by this user and save the form.

.. important::

    These menus require additional permissions to allow webi to access ConfigMgt and external Confd port:

    * :menuselection:`Configuration --> Management --> Media Servers`
    * :menuselection:`Services --> IPBX --> IPBX Settings --> Labels`
    * :menuselection:`Services --> IPBX --> IPBX Settings --> Users`
    * :menuselection:`Services --> IPBX --> Call Management --> Outgoing calls`
    * :menuselection:`Services --> IPBX --> Trunk management --> SIP Protocol`
    * :menuselection:`Services --> IPBX --> Trunk management --> SIP Provider`
    * :menuselection:`Services --> Call Center --> Queues`

    To allow admin users to use these menus, you must also give them the permission
    *Authenticate configuration server web services request (required for admin users)*
    from the :menuselection:`Rules --> IPBX --> Control System` menu.

    .. figure:: authenticate_web_services.png

       :menuselection:`Rules --> IPBX --> Control System`

    You can find detailed explanation in the :ref:`auth-backends-session` authentication backend documentation
    and the :ref:`xivo-auth Developer's Guide <call_to_confd_from_webi>`.
