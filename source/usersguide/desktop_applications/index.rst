.. _desktop-application:

********************
Desktop Applications
********************

The *XiVO Desktop Application* is a standalone executable for either :ref:`uc-assistant`, :ref:`switchboard` and :ref:`agent`. It is available for Windows (64bits) and Linux
Debian based distributions (64bits). It offers some additional features compares to the existing web version that can be run in a browser.

.. toctree::
   :maxdepth: 2

   installation
   configuration
   specific_features
