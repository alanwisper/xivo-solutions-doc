.. _chat_api:

********
CHAT API
********

Allows user to send a text message to another user .
If `xivo-chat-backend` package is installed  messages can be persisted (even if user is disconnected).

A sample of implementation is available in `app/assets/javascripts/pages/sampleFlashText.js`
and `app/views/sample/sampleFlashText.scala.html`


Chat Methods
============

Introduction
------------

All chat methods contains `sequence` number that allows you setting any value to correlate your command with the answer.
Basically if you send 3 chat messages to 3 different users, you will be able to know which acknowledgment is related to which destination just by looking at this number.

Send chat message
-----------------

Cti.sendFlashTextMessage(username, sequence_number, message)

Example :

::

    Cti.sendFlashTextMessage("user", 24, "This is my message");

Retrieve chat history
---------------------

Cti.getFlashTextPartyHistory(username, sequence_number)

Example :

::

    Cti.getFlashTextPartyHistory("user", 24);

FlashText Events
================

* Handler on : Cti.MessageType.FLASHTEXTEVENT

Message Events
--------------

* FlashTextUserMessage

Received by the user when a message is sent

::

  {
    "msgType": "FlashTextEvent",
    "ctiMessage": {
      "event": "FlashTextUserMessage",
      "from": {
        "username": "bwillis",
        "phoneNumber": "1001",
        "displayName": "Bruce Willis",
        "guid": "goegjutxyt8c5dmsqy6xk4f85w"
      },
      "to": {
        "username": "jbond",
        "phoneNumber": "1000",
        "displayName": "James Bond",
        "guid": "eiifh5zietbi7k8ao6ndrzr7zo"
      },
      "sequence": 145,
      "message": "How are you James ?",
      "date": "2019-05-28T14:55:08.628+02:00",
    }

* FlashTextSendMessageAck

The message sent to the user can be delivered to the user, means the user is connected to the server,
if the connected application is able to receive the message, message will be delivered.

::

  {
    "msgType": "FlashTextEvent",
    "ctiMessage": {
      "event": "FlashTextSendMessageAck",
      "sequence": 1,
      "offline": false,
      "date": "2020-06-10T12:08:19.565Z"
    }
  }


* FlashTextSendMessageAckOffline

If user is just disconnected, message is persisted and available to be retrieved later on further connection.

::

  {
    "msgType": "FlashTextEvent",
    "ctiMessage": {
      "event": "FlashTextSendMessageAckOffline",
      "sequence": 1,
      "offline": true,
      "date": "2020-06-10T12:08:19.565Z"
    }
  }

* FlashTextSendMessageNack

In case of error (network connection, service not running or not available, database error),


::

  {
    "msgType": "FlashTextEvent",
    "ctiMessage": {
      "event":"FlashTextSendMessageNack"
      "sequence":28841,
    }
  }

History Events
--------------

* FlashTextUserMessageHistory

Event sent each time when the chat history between two users is requested

::

  {
  	"msgType": "FlashTextEvent",
  	"ctiMessage": {
  		"event": "FlashTextUserMessageHistory",
  		"users": [{
  			"username": "jbond",
        "phoneNumber": "1000",
  			"displayName": "James Bond",
  			"guid": "eiifh5zietbi7k8ao6ndrzr7zo"
  		}, {
  			"username": "bwillis",
        "phoneNumber": "1001",
  			"displayName": "Bruce Willis",
  			"guid": "goegjutxyt8c5dmsqy6xk4f85w"
  		}],
  		"messages": [{
  			"from": {
  				"username": "bwillis",
          "phoneNumber": "1000",
  				"displayName": "Bruce Willis",
  				"guid": "goegjutxyt8c5dmsqy6xk4f85w"
  			},
  			"to": {
  				"username": "jbond",
          "phoneNumber": "1001",
  				"displayName": "James Bond",
  				"guid": "eiifh5zietbi7k8ao6ndrzr7zo"
  			},
  			"sequence": 0,
  			"message": "hello1",
  			"date": "2020-03-06T16:41:18.789Z"
  		}, {
  			"from": {
  				"username": "jbond",
          "phoneNumber": "1000",
  				"displayName": "James Bond",
  				"guid": "eiifh5zietbi7k8ao6ndrzr7zo"
  			},
  			"to": {
  				"username": "bwillis",
          "phoneNumber": "1001",
  				"displayName": "Bruce Willis",
  				"guid": "goegjutxyt8c5dmsqy6xk4f85w"
  			},
  			"sequence": 0,
  			"message": "Hello",
  			"date": "2020-03-09T10:19:12.763Z"
  		}],
  		"sequence": 1
  	}
  }
