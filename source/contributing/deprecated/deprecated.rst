*********************
Deprecated Features
*********************

.. warning:: This section lists deprecated features. 

.. toctree::
	:maxdepth: 2
	
	centralized_user_management/index

